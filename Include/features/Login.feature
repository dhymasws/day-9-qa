#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@featureLogin
Feature: Login success

	Background:
  	Given I open https://www.saucedemo.com 

  @scenarioLogin
  Scenario Outline: Logging in as an existing user with valid account
    When I fill in correct  username <username> and correct password <password>
		And I click LOGIN
		Then I see a homepage

    Examples: 
      | username | password | 
      | standard_user | secret_sauce | 
      | locked_out_user | secret_sauce | 
      
 
	@scenarioLoginFailure
	Scenario Outline: Logging in as an existing user with invalid password
		When I fill correct username <username> and incorrect password <password>
		And I click LOGIN
		Then I see an error message 'Epic sadface: Username and password do not match any user in this service'
		
		Examples: 
      | username | password | 
      | standard_user | 1674dua | 
      | locked_out_user | 1234456adaj | 

	@scenarioLoginNullPassword
	Scenario Outline: Logging with null password
		When I fill username <username> and null password <password>
		And I click LOGIN 
		Then I see an error message 'Epic sadface: Password is required'
			
		Examples: 
      | username | password | 
      | standard_user || 
      | locked_out_user || 
      
	@scenarioLoginNullUsername
	Scenario Outline: Loggin with null username
		When I fill null username <username> and random password <password>
		And I click LOGIN
		Then I see an error message 'Epic sadface: Username is required'
		
		Examples:
			| username | password | 
      || adaihcac | 
      || asfasfaf |
	
			
	
	
	
	
	
	
	
	
	