import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Login {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I open https://www.saucedemo.com ")
	def openInstagram() {
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://www.saucedemo.com/')
	}

	@When("I fill in correct  username (.*) and correct password (.*)")
	def enterCorrectPassword(String username, String password) {
		WebUI.setText(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'), 'username')

		WebUI.setEncryptedText(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'),
				'password')
	}

	@When("I click LOGIN")
	def clickLogin() {
		WebUI.click(findTestObject('Object Repository/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_0dff71'))
	}

	@Then("I see a homepage")
	def homepage() {
		WebUI.closeBrowser()
	}

	@When("I fill correct username (.*) and incorrect password (.*)")
	def enterIncorrectPassword(String username, String password) {
		WebUI.setText(findTestObject('Object Repository/Login_Failure/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_db77ac'),
				'standard_user')

		WebUI.setEncryptedText(findTestObject('Object Repository/Login_Failure/Page_Swag Labs/input_standard_userlocked_out_userproblem_u_3423e9'),
				'vRERGFELFBY=')
	}
	@Then("I see an error message 'Epic sadface: Username and password do not match any user in this service'")
	def incorrectMessage() {
		WebUI.click(findTestObject('Object Repository/Login_Failure/Page_Swag Labs/h3_Epic sadface Username and password do no_0e8909'))
		WebUI.closeBrowser()
	}
}